provider "aws" {
        region="us-east-1"
}

resource "aws_vpc" "vpc" {
        cidr_block=var.vpc_cidr
        enable_dns_hostnames=true

tags= {
        Name="vpc"
}
}

resource "aws_subnet" "public_subnets" {
        vpc_id=aws_vpc.vpc.id
        count=length(var.public_subnet_cidrs)
        cidr_block =element(var.public_subnet_cidrs,count.index)
        availability_zone=element(var.azs,count.index)

tags = {
        Name="vpc_public_subnet ${count.index+1}-${var.azs[count.index]}"
}
}

resource "aws_subnet" "private_subnets" {
        vpc_id=aws_vpc.vpc.id
        count=length(var.private_subnet_cidrs)
        cidr_block =element(var.private_subnet_cidrs,count.index)
        availability_zone=element(var.azs,count.index)
tags = {
        Name="vpc_private Subnet ${count.index+1}-${var.azs[count.index]}"
}
}

resource "aws_subnet" "private_subnets_database" {
        vpc_id=aws_vpc.vpc.id
        count=length(var.private_subnet_database_cidrs)
        cidr_block =element(var.private_subnet_database_cidrs,count.index)
        availability_zone=element(var.azs,count.index)
tags = {
        Name="vpc_private database Subnet ${count.index+1}-${var.azs[count.index]}"

}
}

resource "aws_internet_gateway" "vpc_igw" {
        vpc_id=aws_vpc.vpc.id
tags={
        name="internet gateway"
}
}
resource "aws_route_table" "public_route" {
 vpc_id = aws_vpc.vpc.id

 route {
   cidr_block = "0.0.0.0/0"
 }

 tags = {
   Name = "public route table"
 }
}
