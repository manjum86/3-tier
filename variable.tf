variable "vpc_cidr" {
        default="10.0.0.0/16"
}

variable "public_subnet_cidrs" {
 type        = list(string)
 description = "Public Subnet CIDR values"
 default     = ["10.0.0.0/21", "10.0.8.0/21"]
}

variable "private_subnet_cidrs" {
 type        = list(string)
 description = "Private Subnet CIDR values"
 default     = ["10.0.16.0/21", "10.0.24.0/21"]
}

variable "private_subnet_database_cidrs" {
 type        = list(string)
 description = "Private Subnet CIDR values for DB"
 default     = ["10.0.32.0/21","10.0.40.0/21"]
}

variable "azs" {
type = list(string)
description= "Availability Zones"
default =["us-east-1a","us-east-1b"]
}
